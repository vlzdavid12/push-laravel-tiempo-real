<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Pusher\Pusher;


class NotificationController extends Controller
{
    public static function options(): array
    {
        return array(
            'cluster' => env('PUSHER_APP_CLUSTER'),
            'useTLS' => true
        );
    }

    public static function pusher(): Pusher
    {
        return new Pusher(
            env('PUSHER_APP_KEY'),
            env('PUSHER_APP_SECRET'),
            env('PUSHER_APP_ID'),
            self::options()
        );
    }


    public static function notify()
    {
        $data['message'] = "Hello Push Code";
        self::pusher()->trigger('notify-chanel', 'my-event', $data);
    }


}
